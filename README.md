# MelodySmart iOS Example (Objective-C)

This repository contains the Objective-C sample app and library, which allow any iOS device to communicate with BlueCreation's BC118/BC127/BC139 family of products over a Bluetooth Low Energy link.