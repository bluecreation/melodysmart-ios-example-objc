//
//  MSMasterViewController.m
//  MelodySmart Example v2
//
//  Created by Stanislav Nikolov on 23/07/2014.
//  Copyright (c) 2014 Blue Creation. All rights reserved.
//

#import "DeviceListViewController.h"
#import "DeviceViewController.h"
#import "MelodyManager.h"

@interface DeviceListViewController() <MelodyManagerDelegate> {
    NSMutableArray *_objects;
    MelodyManager *melodyManager;
}

@end

@implementation DeviceListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    melodyManager = [MelodyManager new];
    [melodyManager setForService:nil andDataCharacterisitc:nil andPioReportCharacteristic:nil andPioSettingCharacteristic:nil];
    melodyManager.delegate = self;

    NSLog(@"Using MelodyManager v%@", melodyManager.version);
    
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Rescan" style:UIBarButtonItemStylePlain target:self action:@selector(scan)];

    self.detailViewController = (DeviceViewController*)[[self.splitViewController.viewControllers lastObject] topViewController];
}

- (void)viewDidAppear:(BOOL)animated {
    [self scan];
}

- (void)viewDidDisappear:(BOOL)animated {
    [melodyManager stopScanning];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scan {
    [self clearObjects];
    [melodyManager scanForMelody];
}

- (void)clearObjects {
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (int i = 0; i < _objects.count; i++) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    [_objects removeAllObjects];
    [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)insertNewObject:(MelodySmart*)device
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects addObject:device];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_objects.count - 1 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    MelodySmart *device = _objects[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"name: %@, rssi: %@", device.name, device.RSSI];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"data: %@", device.manufacturerAdvData.description];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.detailViewController.melodySmart = _objects[indexPath.row];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showDetail"]) {
        DeviceViewController *destination = segue.destinationViewController;
        destination.melodySmart = _objects[[self.tableView indexPathForSelectedRow].row];
    } 
}

#pragma mark MelodyManager delegate

-(void)melodyManagerDiscoveryDidRefresh:(MelodyManager *)manager {
//    NSLog(@"discoveryDidRefresh");
    for (int i = _objects.count; i < [MelodyManager numberOfFoundDevices]; i++) {
        [self insertNewObject:[MelodyManager foundDeviceAtIndex:i]];
    }
    [self.tableView reloadData];
}

@end
