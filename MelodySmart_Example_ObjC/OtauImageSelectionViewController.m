//
//  OtauImageSelectionViewController.m
//  MelodySmart_Example_ObjC
//
//  Created by Stanislav Nikolov on 22/01/2015.
//  Copyright (c) 2015 BlueCreation. All rights reserved.
//

#import "OtauImageSelectionViewController.h"

@implementation MelodyRelease
@end

@interface OtauImageSelectionViewController ()

@end

@implementation OtauImageSelectionViewController {
    NSMutableArray *melodyReleases;
    UIAlertView *loadingDialog;
}

#define BASE_URL [NSURL URLWithString:@"https://bluecreation.com/stan/melody_smart_otau/"]

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Choose a FW image";

    loadingDialog = [[UIAlertView alloc] initWithTitle:@"Loading FW versions" message:@"Please wait..." delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [loadingDialog show];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, (unsigned long)NULL), ^(void) {
        NSData *manifestData = [NSData dataWithContentsOfURL:[BASE_URL URLByAppendingPathComponent:@"manifest.json"]];

        NSDateFormatter *formatter = [NSDateFormatter new];
        formatter.dateFormat = @"yyyy-MM-dd";

        melodyReleases = [NSMutableArray array];

        if (!manifestData) return;

        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:manifestData options:0 error:nil];
        NSString *defaultKeyfile = jsonObject[@"default_keyfile"];

        NSArray *releases = jsonObject[@"releases"];
        for (NSDictionary *release in releases) {
            MelodyRelease *melodyRelease = [MelodyRelease new];
            melodyRelease.version           = release[@"version"];
            melodyRelease.imageFileUrl      = [BASE_URL URLByAppendingPathComponent:release[@"file_name"]];
            melodyRelease.keyFileUrl        = [BASE_URL URLByAppendingPathComponent:(release[@"keyfile"] ? release[@"keyfile"] : defaultKeyfile)];
            melodyRelease.releaseDate       = [formatter dateFromString:release[@"release_date"]];
            [melodyReleases addObject:melodyRelease];
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            [loadingDialog dismissWithClickedButtonIndex:0 animated:YES];
            [self.tableView reloadData];
        });
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return melodyReleases.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier" forIndexPath:indexPath];

    MelodyRelease *release = melodyReleases[indexPath.row];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateStyle:NSDateFormatterShortStyle];

    cell.textLabel.text = release.version;
    cell.detailTextLabel.text = [formatter stringFromDate:release.releaseDate];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _delegate.melodyRelease = melodyReleases[indexPath.row];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
