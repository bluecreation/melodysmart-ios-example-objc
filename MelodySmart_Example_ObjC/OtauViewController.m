//
//  OtauViewController.m
//  MelodySmart_Example_ObjC
//
//  Created by Stanislav Nikolov on 22/01/2015.
//  Copyright (c) 2015 BlueCreation. All rights reserved.
//

#import "OtauViewController.h"
#import "OtauImageSelectionViewController.h"

@interface OtauViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblDeviceVersion;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedVersion;
@property (weak, nonatomic) IBOutlet UILabel *lblProgress;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnStartOtau;
@property (weak, nonatomic) IBOutlet UIProgressView *pvProgress;

@end

@implementation OtauViewController {
    MelodyRelease *_melodyRelease;
    NSData *imageData;
    NSData *keyFileData;
}

- (void)setMelodyRelease:(MelodyRelease *)melodyRelease {
    _melodyRelease = melodyRelease;
    [self updateUi];
}

- (MelodyRelease *)melodyRelease {
    return _melodyRelease;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self updateUi];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateUi {
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateStyle = NSDateFormatterShortStyle;
    if (_melodyRelease) {
        _lblSelectedVersion.text = [NSString stringWithFormat:@"Selected FW: %@ @ %@", _melodyRelease.version, [formatter stringFromDate:_melodyRelease.releaseDate]];
    }
    _btnStartOtau.enabled = _melodyRelease != nil;
    _lblDeviceVersion.text = [NSString stringWithFormat:@"Device FW: %@", _melodySmart.firmwareVersion];
}

- (IBAction)btnStartOtau_TouchedUpInside:(id)sender {
    _lblStatus.text = @"Downloading FW image...";

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        imageData = [NSData dataWithContentsOfURL:_melodyRelease.imageFileUrl];
        keyFileData = [NSData dataWithContentsOfURL:_melodyRelease.keyFileUrl];

        if (imageData && keyFileData) {
            dispatch_async(dispatch_get_main_queue(), ^{
                _lblStatus.text = @"Rebooting device...";
                [_melodySmart rebootToOtauMode];
            });
        } else {
            _lblStatus.text = @"Error";
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Error downloading the requested FW, please try again!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    });
}

-(void)didDetectBootMode:(BootMode)bootMode {
    NSLog(@"didDetectBootMode: %d", bootMode);

    switch (bootMode) {
        case BOOT_MODE_APPLICATION:
            [self updateUi];
            break;

        case BOOT_MODE_BOOTLOADER:
            _btnStartOtau.enabled = NO;
            [_melodySmart startOtauWithImageData:imageData keyFileData:keyFileData];
            break;

        default:
            break;
    }
}

- (void)startOtau {
}

- (void)didUpdateOtauState:(OtauState)state percent:(int)percent {
    NSLog(@"didUpdateOtauState:%d percent:%d", state, percent);
    switch (state) {
        case OTAU_IN_PROGRESS:
            _lblStatus.text = @"Update In Progress...";
            _pvProgress.progress = percent / 100.0;
            _lblProgress.text = [NSString stringWithFormat:@"%d%%", percent];
            break;

        case OTAU_COMPLETE:
            _lblStatus.text = @"Update successful!";
            break;

        case OTAU_FAILED:
            _lblStatus.text = @"Update Failed!";
            break;

        case OTAU_IDLE:
            _lblStatus.text = @"Idle";
            break;
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"image_selection"]) {
        ((OtauImageSelectionViewController*)segue.destinationViewController).delegate = self;
    }
}

@end
