//
//  RemoteCommandsViewController.m
//  MelodySmart_Example_ObjC
//
//  Created by Stanislav Nikolov on 22/01/2015.
//  Copyright (c) 2015 BlueCreation. All rights reserved.
//

#import "RemoteCommandsViewController.h"

@interface RemoteCommandsViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *tfCommand;
@property (weak, nonatomic) IBOutlet UITextView *tvOutput;

@end

@implementation RemoteCommandsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_melodySmart sendRemoteCommand:_tfCommand.text];
    return YES;
}

- (void)didReceiveCommandReply:(NSData *)data {
    _tvOutput.text = [_tvOutput.text stringByAppendingString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
    [_tvOutput scrollRangeToVisible:NSMakeRange(_tvOutput.text.length, 1)];
}

@end
