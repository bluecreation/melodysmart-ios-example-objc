//
//  RemoteCommandsViewController.h
//  MelodySmart_Example_ObjC
//
//  Created by Stanislav Nikolov on 22/01/2015.
//  Copyright (c) 2015 BlueCreation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MelodySmart.h"

@interface RemoteCommandsViewController : UIViewController

@property MelodySmart *melodySmart;

- (void)didReceiveCommandReply:(NSData*)data;

@end
