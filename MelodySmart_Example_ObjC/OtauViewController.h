//
//  OtauViewController.h
//  MelodySmart_Example_ObjC
//
//  Created by Stanislav Nikolov on 22/01/2015.
//  Copyright (c) 2015 BlueCreation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MelodySmart.h"

@class MelodyRelease;

@interface OtauViewController : UIViewController

@property MelodySmart *melodySmart;

@property MelodyRelease *melodyRelease;

- (void)didDetectBootMode:(BootMode)bootMode;

- (void)didUpdateOtauState:(OtauState)state percent:(int)percent;

@end
