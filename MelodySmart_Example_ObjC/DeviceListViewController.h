//
//  MSMasterViewController.h
//  MelodySmart Example v2
//
//  Created by Stanislav Nikolov on 23/07/2014.
//  Copyright (c) 2014 Blue Creation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DeviceViewController;

@interface DeviceListViewController : UITableViewController

@property (strong, nonatomic) DeviceViewController *detailViewController;

@end
