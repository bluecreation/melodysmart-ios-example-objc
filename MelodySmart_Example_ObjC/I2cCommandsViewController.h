//
//  I2cCommandsViewController.h
//  MelodySmart_Example_ObjC
//
//  Created by Stanislav Nikolov on 22/01/2015.
//  Copyright (c) 2015 BlueCreation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MelodySmart.h"

@interface I2cCommandsViewController : UIViewController

@property MelodySmart *melodySmart;

- (void)didReceiveI2cReplyWithSuccess:(BOOL)success data:(NSData*)data;

@end
