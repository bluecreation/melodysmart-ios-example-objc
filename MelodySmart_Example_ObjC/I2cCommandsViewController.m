//
//  I2cCommandsViewController.m
//  MelodySmart_Example_ObjC
//
//  Created by Stanislav Nikolov on 22/01/2015.
//  Copyright (c) 2015 BlueCreation. All rights reserved.
//

#import "I2cCommandsViewController.h"

@interface I2cCommandsViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *tfDeviceAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfRegisterAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfOutgoingData;
@property (weak, nonatomic) IBOutlet UITextField *tfIncomingData;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@end

@implementation I2cCommandsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)btnWrite_TouchUpInside:(id)sender {
    _lblStatus.text = @"";
    
    uint8_t deviceAddr;
    NSData *registerAddress;

    if (![self validateDeviceAddress:&deviceAddr registerAddress:&registerAddress]) {
        return;
    }

    NSMutableData *fullData = [NSMutableData dataWithData:registerAddress];
    
    NSData *payload = [self dataWithHexString:_tfOutgoingData.text];
    if (!payload || payload.length > 16) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"The outgoing data should consist of an even number of hex digits and be no more than 19 bytes!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }

    [fullData appendData:payload];
    
    _lblStatus.text = @"Writing...";

    [_melodySmart writeI2cData:fullData toDevAddress:deviceAddr];
}

- (IBAction)btnRead_TouchUpInside:(id)sender {
    _lblStatus.text = @"";
    
    uint8_t deviceAddr;
    NSData *regAddr;

    if (![self validateDeviceAddress:&deviceAddr registerAddress:&regAddr]) {
        return;
    }
    
    _lblStatus.text = @"Reading...";
    
    const int length = 16;

    [_melodySmart readI2cDataFromDevAddress:deviceAddr writePortion:regAddr length:length];
}

- (void)didReceiveI2cReplyWithSuccess:(BOOL)success data:(NSData *)data {
    NSLog(@"didReceiveI2cReplyWithSuccess: %d data: %@", success, data);

    _lblStatus.text = [NSString stringWithFormat:@"Operaion %@!",
                       success ? @"successful" : @"failed"];
    
    NSString *dataString = data.description;
    _tfIncomingData.text = [dataString substringWithRange:NSMakeRange(1, dataString.length - 2)];
}

- (BOOL)validateDeviceAddress:(uint8_t*)deviceAddress registerAddress:(NSData**)registerAddress {
    NSScanner *scanner;
    
    unsigned devAddr;
    if (_tfDeviceAddress.text.length != 2) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"The device address should consist of 2 hex digits!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return NO;
    }
    scanner = [NSScanner scannerWithString:_tfDeviceAddress.text];
    [scanner scanHexInt:&devAddr];
    *deviceAddress = devAddr;

    NSData *regAddr = [self dataWithHexString:_tfRegisterAddress.text];
    if (!regAddr) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"The register address should consist of an even number of hex digits!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return NO;
    }
    *registerAddress = regAddr;

    return YES;
}

- (NSData*)dataWithHexString:(NSString *)hex {
    char buf[3] = { 0 };

    hex = [hex stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (hex.length % 2 != 0) return nil;
    
    unsigned char bytes[hex.length / 2];
    unsigned char *bp = bytes;
    for (int i = 0; i < hex.length; i += 2) {
        buf[0] = [hex characterAtIndex:i];
        buf[1] = [hex characterAtIndex:i+1];
        char *b2 = NULL;
        *bp++ = strtol(buf, &b2, 16);
        
        if (b2 != buf + 2) return nil;
    }
    
    return [NSData dataWithBytes:bytes length:sizeof(bytes)];
}

@end
