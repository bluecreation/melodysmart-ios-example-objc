//
//  MSDetailViewController.h
//  MelodySmart Example v2
//
//  Created by Stanislav Nikolov on 23/07/2014.
//  Copyright (c) 2014 Blue Creation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MelodySmart.h"

@interface DeviceViewController : UIViewController

@property (strong, nonatomic) MelodySmart *melodySmart;

@property (weak, nonatomic) IBOutlet UITextField *tfIncomingData;
@property (weak, nonatomic) IBOutlet UITextField *tfOutgoingData;

@end
