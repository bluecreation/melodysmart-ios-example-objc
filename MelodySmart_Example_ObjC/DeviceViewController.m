//
//  MSDetailViewController.m
//  MelodySmart Example v2
//
//  Created by Stanislav Nikolov on 23/07/2014.
//  Copyright (c) 2014 Blue Creation. All rights reserved.
//

#import "DeviceViewController.h"

#import "I2cCommandsViewController.h"
#import "RemoteCommandsViewController.h"
#import "OtauViewController.h"

@interface DeviceViewController () <MelodySmartDelegate, UITextFieldDelegate>

@end

@implementation DeviceViewController {
    __weak I2cCommandsViewController *i2cController;
    __weak RemoteCommandsViewController *commandsController;
    __weak OtauViewController *otauController;
}

#pragma mark - Managing the detail item

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.melodySmart.name;

    self.melodySmart.delegate = self;
    
    [self.melodySmart connect];
}

-(void)dealloc {
    [self.melodySmart disconnect];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MelodySmart delegate

- (void)melodySmart:(MelodySmart *)melody didConnectToMelody:(BOOL)result {
    NSLog(@"didConnectToMelody");
}

-(void)melodySmart:(MelodySmart *)melody didSendData:(NSError *)error {
    
}

-(void)melodySmart:(MelodySmart *)melody didReceiveData:(NSData *)data {
    self.tfIncomingData.text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

-(void)melodySmartDidDisconnectFromMelody:(MelodySmart *)melody {
}

- (void)melodySmart:(MelodySmart *)melody didReceivePioChange:(unsigned char)state WithLocation:(BcSmartPioLocation)location {
}

- (void)melodySmart:(MelodySmart *)melody didReceiveI2cReplyWithSuccess:(BOOL)success data:(NSData *)data {
    [i2cController didReceiveI2cReplyWithSuccess:success data:data];
}

- (void)melodySmart:(MelodySmart *)melody didReceiveCommandReply:(NSData *)data {
    [commandsController didReceiveCommandReply:data];
}

- (void)melodySmart:(MelodySmart *)melody didDetectBootMode:(BootMode)bootMode {
    [otauController didDetectBootMode:bootMode];
}

- (void)melodySmart:(MelodySmart *)melody didUpdateOtauState:(OtauState)state withProgress:(int)percent {
    [otauController didUpdateOtauState:state percent:percent];
}

#pragma mark - UI Handling

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (![self.melodySmart sendData:[textField.text dataUsingEncoding:NSUTF8StringEncoding]]) {
        NSLog(@"Send error");
    }

    return YES;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"i2c"]) {
        i2cController = segue.destinationViewController;
        i2cController.melodySmart = self.melodySmart;
    } else if ([segue.identifier isEqualToString:@"commands"]) {
        commandsController = segue.destinationViewController;
        commandsController.melodySmart = self.melodySmart;
    } else if ([segue.identifier isEqualToString:@"otau"]) {
        otauController = segue.destinationViewController;
        otauController.melodySmart = self.melodySmart;
    }
}

@end
