//
//  OtauImageSelectionViewController.h
//  MelodySmart_Example_ObjC
//
//  Created by Stanislav Nikolov on 22/01/2015.
//  Copyright (c) 2015 BlueCreation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OtauViewController.h"

@interface MelodyRelease : NSObject

@property NSString *version;
@property NSURL *imageFileUrl;
@property NSURL *keyFileUrl;
@property NSDate *releaseDate;

@end

@interface OtauImageSelectionViewController : UITableViewController

@property (weak) OtauViewController *delegate;

@end
